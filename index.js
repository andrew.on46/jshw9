const buttonsList = document.querySelectorAll(".tabs-title");
const contentList = document.querySelectorAll(".tab-li");

buttonsList.forEach((button) => {
  button.addEventListener("click", (e) => {
    buttonsList.forEach((btn) => {
      if (e.target === btn) {
        btn.classList.add("active");
      } else {
        btn.classList.remove("active");
      }
    });

    contentList.forEach((contentItem) => {
      if (e.target.dataset.title === contentItem.dataset.title) {
        contentItem.classList.replace("hidden", "block");
      } else {
        contentItem.classList.replace("block", "hidden");
      }
    });
  });
});
